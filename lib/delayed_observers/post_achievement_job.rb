class PostAchievementJob < Struct.new(:post)
  def perform
    if post.user(true).posts_count == 5
      #shoot blind and assume they do not have existing achievement because we are checking an exact number
      achievement = Achievement.find_by_slug('freq-fly')

      if !post.user.achievements.include?(achievement)
        post.user.rep = post.user.rep + achievement.rep_bonus

        post.user.save!
        post.user.achievements << achievement
      end
    end
  end
end