class CommentVoteRepObserver < ActiveRecord::Observer
  observe :comment_vote

  def before_save(vote)
    #add rep to author
    author = vote.comment.post.user
    author.rep = author.rep + vote.vote

    if !author.save
      return false
    end

    #the voter also gets a ding.
    if vote.vote == -1
      vote.user.rep = vote.user.rep + vote.vote

      if !vote.user.save
        return false
      end
    end
  end
end