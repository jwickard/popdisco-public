class PostVoteRepObserver < ActiveRecord::Observer
  observe :post_vote

  def before_save(vote)

    if !vote.post.update_attributes(vote_count_cache: vote.post.vote_count_cache + vote.vote)
      return false
    end


    #add rep to author
    author = vote.post.user
    author.rep = author.rep+vote.vote

    if !author.save
      return false
    end



    #add rep to any author organizations
    if author.organizations
      author.organizations.each do |org|
        org.rep = org.rep + vote.vote

        if !org.save
          return false
        end
      end
    end

    #if the vote is negative, ding the voter
    if vote.vote == -1
      vote.user.rep = vote.user.rep - 1
      if !vote.user.save
        return false
      end
    end
  end
end
