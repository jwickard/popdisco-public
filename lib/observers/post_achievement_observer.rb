class PostAchievementObserver < ActiveRecord::Observer
  observe :post

  def after_save(post)
    Delayed::Job.enqueue PostAchievementJob.new(post)
  end
end
