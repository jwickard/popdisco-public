Popdisco::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  #resources :comments

  #posts routing
  get 'posts/popular' => 'posts#popular_posts', :as => :popular_posts

  resources :posts do
    resources :comments
  end

  get 'organizations/recent' => 'organizations#recent_organizations', :as => :recent_organizations
  get 'organizations/reputation' => 'organizations#top_organizations', :as => :top_organizations
  get 'organizations/:id/join' => 'organizations#join', :as => :join_organization

  resources :organizations, :except => :destroy

  get "omniauth_callbacks/facebook"

  devise_for :users, :controllers => { :omniauth_callbacks => 'omniauth_callbacks' }

  resources :user_profiles, except: [:index, :show, :destroy]

  #members routes
  get 'members' => 'members#index'
  get 'members/recent' => 'members#recent_members', :as => :recent_members
  get 'members/reputation' => 'members#members_by_reputation', :as => :top_members
  get 'members/:id' => 'members#show', :as => :member

  #counterpoint routes
  get 'posts/:id/counterpoints/new' => 'posts#new_counterpoint', :as => :new_post_counterpoint
  post 'posts/:id/counterpoints' => 'posts#create_counterpoint', :as => :post_counterpoints

  #tagging routes
  get 'topics/:tag/posts' => 'tags#posts_by_tag', :as => :posts_by_tag
  get 'topics' => 'tags#index'
  get 'topics/recent' => 'tags#recent_tags', :as => :recent_topics
  get 'topics/popular' => 'tags#popular_tags', :as => :popular_topics

  #voting routes
  match 'vote/post/:post_id/up' => 'votes#vote_post_up', :as => :vote_post_up
  match 'vote/post/:post_id/down' => 'votes#vote_post_down', :as => :vote_post_down
  match 'vote/comment/:comment_id/up' => 'votes#vote_comment_up', :as => :vote_comment_up
  match 'vote/comment/:comment_id/down' => 'votes#vote_comment_down', :as => :vote_comment_down

  #achievement routes
  get "achievements" => 'achievements#index', :as => :achievements
  get "achievements/:id" => 'achievements#show', :as => :achievement

  get "achievements/show"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'posts#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
