Paperclip.interpolates :hashed_file_name do |attachment, style|
  attachment.instance.hashed_file_name
end