class CreateAuthentications < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
      t.string :uid
      t.string :provider
      t.string :token
      t.integer :user_id

      t.timestamps
    end

    add_index :authentications, [:uid, :provider], :unique => true
  end
end
