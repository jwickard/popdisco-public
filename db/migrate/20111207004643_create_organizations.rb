class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :description
      t.integer :rep, :null => false, :default => 0
      t.string :link
      t.boolean :enabled, :null => false, :default => false

      t.timestamps
    end

    add_index :organizations, :name, :unique => true
  end
end
