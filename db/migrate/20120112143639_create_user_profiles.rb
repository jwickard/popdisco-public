class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.integer :user_id
      t.string :name
      t.string :website
      t.string :location
      t.timestamp :birthday
      t.text :bio

      t.timestamps
    end
  end
end
