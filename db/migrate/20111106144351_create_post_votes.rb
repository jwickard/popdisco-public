class CreatePostVotes < ActiveRecord::Migration
  def change
    create_table :post_votes do |t|
      t.integer :post_id, :null => false
      t.integer :user_id, :null => false
      t.integer :vote, :null => false

      t.timestamps
    end

    #user can only vote once.
    add_index :post_votes, [:user_id, :post_id], :unique => true
  end
end
