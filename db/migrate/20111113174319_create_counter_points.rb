class CreateCounterPoints < ActiveRecord::Migration
  def change
    create_table :counter_points do |t|
      t.integer :post_id, :null => false
      t.integer :counter_point_id, :null => false

      t.timestamps
    end

    add_index :counter_points, [:post_id, :counter_point_id], :unique => true
  end
end
