class AddSlugAndDescriptionToAchievement < ActiveRecord::Migration
  def change
    add_column :achievements, :slug, :string
    add_column :achievements, :description, :string
  end
end
