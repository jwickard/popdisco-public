class CreateAchievements < ActiveRecord::Migration
  def change
    create_table :achievements do |t|
      t.string :name, :nil => false
      t.integer :required_rep, :nil => false, :default => 0
      t.integer :rep_bonus, :nil => false, :default => 0

      t.timestamps
    end
  end
end
