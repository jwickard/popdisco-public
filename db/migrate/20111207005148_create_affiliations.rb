class CreateAffiliations < ActiveRecord::Migration
  def change
    create_table :affiliations do |t|
      t.integer :user_id
      t.integer :organization_id
      t.integer :rep, :null => false, :default => 0
      t.boolean :admin, :null => false, :default => false
      t.boolean :owner, :null => false, :default => false

      t.timestamps
    end

    add_index :affiliations, [:user_id, :organization_id], :unique => true
  end
end
