class AddVoteCountCacheToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :vote_count_cache, :integer, :default => 0
  end
end
