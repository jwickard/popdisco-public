class AddRepToUsers < ActiveRecord::Migration
  def change
    add_column :users, :rep, :integer, :null => false, :default => 0
  end
end
