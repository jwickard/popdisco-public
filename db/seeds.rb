# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

republican = User.create(:email => 'republican@example.com', :username => 'joe.republican', :password => 'smokey', :password_confirmation => 'smokey')
republican.confirm!

democrat = User.create(:email => 'democrat@example.com', :username => 'joe.democrat', :password => 'smokey', :password_confirmation => 'smokey')
democrat.confirm!

independent = User.create(:email => 'independent@example.com', :username => 'joe.independent', :password => 'smokey', :password_confirmation => 'smokey')
independent.confirm!

libertarian = User.create(:email => 'libertarian@example.com', :username => 'joe.libertarian', :password => 'smokey', :password_confirmation => 'smokey')
libertarian.confirm!

liberal = User.create(:email => 'liberal@example.com', :username => 'joe.liberal', :password => 'smokey', :password_confirmation => 'smokey')
liberal.confirm!

authors = [republican, democrat, independent, libertarian, liberal]

tags = ['Obama', 'Economics', 'Republican', 'Conservative', 'Liberal', 'Reagan', 'Congress', 'Deficit', 'Economy', 'Cain', 'Taxes', 'Class warfare']

(1..100).each do |n|
  rand_title = tags[rand(tags.size)]
  post = Post.create(:title => "Post #{n} about #{rand_title}", :body => "Post body for post #{n}", :tag_list => "#{tags[rand(tags.size)]}, #{tags[rand(tags.size)]}, #{tags[rand(tags.size)]}", :user => authors[rand(authors.size)], :created_at => (Time.now - n.day))
end