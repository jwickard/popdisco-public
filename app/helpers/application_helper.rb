module ApplicationHelper
  def recent_tags
    #right now we are just going to list all tags.

    # @param [Hash] options Options:
    #                       * :start_at   - Restrict the tags to those created after a certain time
    #                       * :end_at     - Restrict the tags to those created before a certain time
    #                       * :conditions - A piece of SQL conditions to add to the query
    #                       * :limit      - The maximum number of tags to return
    #                       * :order      - A piece of SQL to order by. Eg 'tags.count desc' or 'taggings.created_at desc'
    #                       * :at_least   - Exclude tags with a frequency less than the given value
    #                       * :at_most    - Exclude tags with a frequency greater than the given value
    #                       * :on         - Scope the find to only include a certain context

    Post.all_tag_counts(start_at: (Time.now - 1.day), order: 'count desc, tags.name asc', limit: 10)
  end
end
