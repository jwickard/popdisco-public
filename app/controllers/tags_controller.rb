class TagsController < ApplicationController
  def index
    #right now we are just going to list all tags.

    # @param [Hash] options Options:
    #                       * :start_at   - Restrict the tags to those created after a certain time
    #                       * :end_at     - Restrict the tags to those created before a certain time
    #                       * :conditions - A piece of SQL conditions to add to the query
    #                       * :limit      - The maximum number of tags to return
    #                       * :order      - A piece of SQL to order by. Eg 'tags.count desc' or 'taggings.created_at desc'
    #                       * :at_least   - Exclude tags with a frequency less than the given value
    #                       * :at_most    - Exclude tags with a frequency greater than the given value
    #                       * :on         - Scope the find to only include a certain context

    #most popular is our default sort
    @topics = Post.tag_counts_on(:tags,  :order => 'tags.name asc').page(params[:page]).per(24)
    @sort_title = 'All'

    respond_to do |format|
      format.html #render index
      format.json { render json: @topics }
    end
  end

  def recent_tags

    @topics = Post.all_tag_counts(start_at: (Time.now - 1.day), order: 'count desc, tags.name asc').page(params[:page]).per(24)
    @sort_title = 'Recent'

    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @topics }
    end
  end

  def popular_tags
    @topics = Post.all_tag_counts(order: 'count desc, tags.name asc').page(params[:page]).per(24)
    @sort_title = 'Popular'

    respond_to do |format|
      format.html { render action: 'index'}
      format.json { render json: @topics }
    end
  end


  def posts_by_tag
    @posts = Post.tagged_with(params[:tag]).page params[:page]
    @topic = params[:tag]

    respond_to do |format|
      format.html # render template
      format.json { render json: @posts }
    end
  end
end
