class OrganizationsController < ApplicationController
  before_filter :authenticate_user!, :only =>  [:join, :new, :create, :edit, :update]
  before_filter :require_organization_admin, only: [:edit, :update]

  # GET /organizations
  # GET /organizations.json
  def index
    @organizations = Organization.where('enabled = ?', true).order('name asc').all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @organizations }
    end
  end

  def recent_organizations
    @organizations = Organization.where('enabled = ?', true).order('created_at desc, name asc').all()

    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @organization }
    end
  end

  def top_organizations
    @organizations = Organization.where('enabled = ?', true).order('rep desc, name asc').all()

    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @organizations }
    end
  end

  def join
    @organization = Organization.find(params[:id])
    @affiliation = @organization.affiliations.build(user_id: current_user.id, owner: false, admin: false)

    respond_to do |format|
      if @affiliation.save
        format.html { redirect_to @organization, notice: "You have signed up to be a member of #{@organization.name}" }
        format.json { render json: @organization }
      else
        format.html { redirect_to @organization, alert: @affiliation.errors.full_messages[0] }
        format.json { render json: @affiliation.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
    @organization = Organization.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @organization }
    end
  end

  # GET /organizations/new
  # GET /organizations/new.json
  def new
    @organization = Organization.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @organization }
    end
  end

  # GET /organizations/1/edit
  def edit
    @organization = Organization.find(params[:id])
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(params[:organization].merge({enabled: true}))

    #default the creating user to be the owner/admin of the group.
    @organization.affiliations.build(user: current_user, rep: 10, owner: true, admin: true)

    respond_to do |format|
      if @organization.save
        format.html { redirect_to @organization, notice: 'Organization was successfully created.' }
        format.json { render json: @organization, status: :created, location: @organization }
      else
        format.html { render action: "new" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /organizations/1
  # PUT /organizations/1.json
  def update
    @organization = Organization.find(params[:id])

    respond_to do |format|
      if @organization.update_attributes(params[:organization])
        format.html { redirect_to @organization, notice: 'Organization was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def require_organization_admin
    organization = Organization.find(params[:id])

    #affiliation = organization.affiliations.find_by_user_id(current_user.id)

    if not organization.user_can_edit?(current_user)
      redirect_to organization, alert: "You must be an organization admin to edit the organization."
    end
  end
end
