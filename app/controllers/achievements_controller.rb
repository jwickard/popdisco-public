class AchievementsController < ApplicationController
  def index
    @unlocked_achievements = Achievement
                      .select("achievements.id, achievements.name, COUNT(user_achievements.achievement_id) as user_count")
                      .joins("LEFT OUTER JOIN user_achievements ON user_achievements.achievement_id = achievements.id")
                      .group("achievements.id, achievements.name")
                      .having("COUNT(user_achievements.achievement_id) > 0")
                      .all()

    @locked_achievements = Achievement
                      .select("achievements.id, achievements.name, achievements.rep_bonus, COUNT(user_achievements.achievement_id) as user_count")
                      .joins("LEFT OUTER JOIN user_achievements ON user_achievements.achievement_id = achievements.id")
                      .group("achievements.id, achievements.name, achievements.rep_bonus")
                      .having("COUNT(user_achievements.achievement_id) = 0")
                      .all()

    respond_to do |format|
      format.html #render index
      format.json { render json: [@unlocked_achievements, @locked_achievements] }
    end
  end

  def show
    @achievement = Achievement.includes(:users).find(params[:id])

    respond_to do |format|
      format.html #render show
      format.json { render json: @achievement }
    end
  end

end
