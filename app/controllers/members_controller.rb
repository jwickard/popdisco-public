class MembersController < ApplicationController
  def index
    @members = User.all

    respond_to do |format|
      format.html #render template
      format.json { render json: @members }
    end
  end

  def show
    @member = User.find(params[:id])

    respond_to do |format|
      format.html #render template
      format.json { render json: @member }
    end
  end

  def recent_members
    @members = User.order('created_at desc, username asc').all()

    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @members }
    end
  end

  def members_by_reputation
    @members = User.order('rep desc, username asc').all()

    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @members }
    end
  end
end
