class CommentsController < ApplicationController
  before_filter :authenticate_user!, :except => :show

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.find_all_by_post_id(params[:post_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    post = Post.find(params[:post_id])
    @comment = post.comments.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = Comment.new({:post_id => params[:post_id]})


    respond_to do |format|
      format.html # new.html.erb
      #ajax, turn off the layout.
      #format.js {render :layout => false}
      format.json { render json: @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = Comment.find_by_id_and_post_id(params[:id], params[:post_id])
  end


  # POST /comments
  # POST /comments.json
  def create
    post = Post.find(params[:post_id])
    args = params[:comment]
    args['user_id'] = current_user.id
    @comment = post.comments.build(args)

    respond_to do |format|
      if @comment.save
        format.html { redirect_to post_url(post), notice: 'Comment was successfully created.' }
        format.json { render json: @comment, status: :created, location: @comment }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = Comment.find_by_id_and_post_id(params[:id], params[:post_id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to post_comment_path(:post_id => @comment.post_id, :id => @comment.id), notice: 'Comment was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find_by_id_and_post_id(params[:id], params[:post_id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to post_comments_path }
      format.json { head :ok }
    end
  end
end
