class PostsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show, :index, :popular_posts]

  before_filter :requires_author, only: [:edit, :update]

  before_filter :requires_new_author, only: [:new_counterpoint, :create_counterpoint]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.order('created_at desc, vote_count_cache desc').page params[:page]

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end

  def popular_posts
    @posts = Post.order('vote_count_cache desc, created_at desc').page params[:page]

    respond_to do |format|
      format.html { render action: :index }
      format.json { render json: @posts }
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = Post.find(params[:id])

    #increment view_count
    @post.view_count = @post.view_count+1
    @post.save!

    #put the current user into view scope
    @current_user = current_user

    #put an empty comment into scope.
    @comment = Comment.new(:post => @post)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @post = Post.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/1/edit
  def edit
    @post = Post.find(params[:id])
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(params[:post])
    @post.user = current_user

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :ok }
    end
  end

  #counterpoints

  #GET /posts/1/counterpoints/new
  #GET /posts/1/counterpoints/new.json
  def new_counterpoint
    @post = Post.new
    @parent_id = params[:id]

    respond_to do |format|
      format.html #template
      format.json { render json: @post }
    end
  end

  #POST /posts/1/counterpoints
  #POST /posts/1/counterpoints.json
  def create_counterpoint
    original = Post.find(params[:id])

    @post = original.counter_points.build(params[:post].merge({:user => current_user}))
    @post.counter_points << original

    respond_to do |format|
      #saving the original wraps everything up in one transaction.
      if original.save
        format.html { redirect_to @post, notice: 'Counter Point was successfully created.' }
        format.json { render json: @post, status: :created, location: @post }
      else
        @parent_id = original.id
        format.html { render action: "new_counterpoint" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def requires_author
    post = Post.find(params[:id])

    if not post.user_can_edit?(current_user)
      redirect_to post, alert: "You must be the article author to edit an article!"
    end
  end

  def requires_new_author
    post = Post.find(params[:id])

    if not post.user_can_post_counterpoint?(current_user)
      redirect_to post, alert: "You cannot create a counterpoint for your own post!"
    end
  end
end
