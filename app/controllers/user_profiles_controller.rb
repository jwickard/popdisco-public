class UserProfilesController < ApplicationController
  before_filter :authenticate_user!

  # GET /user_profiles/new
  # GET /user_profiles/new.json
  def new
    @user_profile = UserProfile.new

    respond_to do |format|
      if not current_user.user_profile.nil?
        format.html { redirect_to edit_user_profile_path(current_user.user_profile), notice: 'User profile exists, editing current' }
        format.json { render json: current_user.user_profile }
      else
        format.html # new.html.erb
        format.json { render json: @user_profile }
      end
    end
  end

  # GET /user_profiles/1/edit
  def edit
    @user_profile = UserProfile.find(params[:id])
  end

  # POST /user_profiles
  # POST /user_profiles.json
  def create
    @user_profile = UserProfile.new(params[:user_profile])
    @user_profile.user = current_user

    respond_to do |format|
      if @user_profile.save
        format.html { redirect_to member_path(current_user), notice: 'User profile was successfully created.' }
        format.json { render json: @user_profile, status: :created, location: @user_profile }
      else
        format.html { render action: "new" }
        format.json { render json: @user_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /user_profiles/1
  # PUT /user_profiles/1.json
  def update
    @user_profile = UserProfile.find(params[:id])

    if params[:avatar]
      #params[:avatar][:md5] = Digest::MD5.file(params[:avatar].tempfile.path).hexdigest
      #params[:avatar].orginal_filename = 'something.jpg'
      #logger.info("new-name: #{Digest::MD5.file(params[:avatar].tempfile.path).hexdigest}")
      @user_profile.user.avatar = params[:avatar]
    end

    respond_to do |format|
      if @user_profile.update_attributes(params[:user_profile]) and @user_profile.user.save
        format.html { redirect_to member_path(current_user), notice: 'User profile was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @user_profile.errors, status: :unprocessable_entity }
      end
    end
  end
end
