class VotesController < ApplicationController

  before_filter :authenticate_user!

  def vote_post_up
    post = Post.find(params[:post_id])

    vote = post.post_votes.build(user: current_user, vote: 1)

    #we want to create a post-vote and update the author in one unit of work.
    respond_to do |format|
      if vote.save
        format.html { redirect_to post, notice: 'Post was successfully voted up!' }
        format.json { render json: vote, status: :created, location: vote  }
      else
        format.html { redirect_to post, alert: 'Post vote failed!' }
        format.json { render json: vote.errors, status: :unprocessable_entity }
      end
    end
  end

  def vote_post_down
    post = Post.find(params[:post_id])

    vote = post.post_votes.build(user: current_user, vote: -1)

    #we want to create a post-vote and update the author in one unit of work.
    respond_to do |format|
      if vote.save
        format.html { redirect_to post, notice: 'Post was successfully voted down!' }
        format.json { render json: vote, status: :created, location: vote  }
      else
        format.html { redirect_to post, alert: 'Post vote failed!' }
        format.json { render json: post.errors, status: :unprocessable_entity }
      end
    end
  end

  def vote_comment_up
    comment = Comment.find(params[:comment_id])

    vote = comment.comment_votes.build(user: current_user, vote: 1)

    respond_to do |format|
      if vote.save
        format.html { redirect_to comment.post, notice: 'Comment was successfully voted up!' }
        format.json { render json: comment, status: :created, location: comment}
      else
        format.html { redirect_to comment.post, alert: 'Comment vote failed!' }
        format.json { render json: comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def vote_comment_down
    comment = Comment.find(params[:comment_id])

    vote = comment.comment_votes.build(user: current_user, vote: -1)

    respond_to do |format|
      if vote.save
        format.html { redirect_to comment.post, notice: 'Comment was successfully voted down!' }
        format.json { render json: comment, status: :created, location: comment}
      else
        format.html { redirect_to comment.post, alert: 'Comment vote failed!' }
        format.json { render json: comment.errors, status: :unprocessable_entity }
      end
    end
  end
end
