class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :post
  has_many :comment_votes
  validates_presence_of :body

  def up_vote_count
    CommentVote.count(:conditions => "comment_id = #{id} and vote = 1")
  end

  def down_vote_count
    CommentVote.count(:conditions => "comment_id = #{id} and vote = -1")
  end

  def user_can_vote?(voter)
    if voter.nil? or voter.id == user_id
      return false
    end

    comment_votes.each do |vote|
      if vote.user_id == voter.id
        return false
      end
    end

    true
  end
end
