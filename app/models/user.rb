class User < ActiveRecord::Base
  has_one :user_profile
  has_many :authentications
  has_many :posts
  has_many :comment_votes
  has_many :post_votes
  has_many :affiliations, :conditions => "enabled = true"
  has_many :organizations, :through => :affiliations
  has_many :user_achievements
  has_many :achievements, :through => :user_achievements

   has_attached_file :avatar,
                     :styles => { original: "128x128>", icon: "48x48>", micro: "32x32>" },
                     :path => ":rails_root/public/system/:attachment/:id/:style/:hashed_file_name",
                     :url => "/system/:attachment/:id/:style/:hashed_file_name"

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  #unique
  validates_uniqueness_of :username
  validates_presence_of :username

  # Setup accessible (or protected) attributes for your model
  attr_accessible :username, :email, :password, :password_confirmation, :remember_me, :rep

  def self.find_for_facebook_oauth(oauth, signed_in_user=nil)

    if signed_in_user
      if signed_in_user.email.nil? or signed_in_user.email.eql?('')
        signed_in_user.update_attributes(:email => oauth['user_info']['email'])
      end

      if auth = signed_in_user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])
        auth.update_attributes(:token => oauth['credentials']['token'])
      else
        signed_in_user.authentications.build(:uid => oauth['uid'], :provider => oauth['provider'], :token => oauth['credentials']['token'])
      end

      return signed_in_user
    else
      if oauth['extra']['user_hash']['verified']
        #try to find on existing facebook authentication or matching email
        user = User.joins('LEFT OUTER JOIN authentications ON authentications.user_id = users.id').where("email = :email or (authentications.provider = :provider and authentications.uid = :uid)", {:email => oauth['extra']['user_hash']['email'], :provider => oauth['provider'], :uid => oauth['uid']}).readonly(false).first
      else
        #otherwise only match on authentication
        user = User.joins(:authentications).where("authentications.provider = :provider and authentications.uid = :uid", {:provider => oauth['provider'], :uid => oauth['uid']}).first
      end

      if user
        #update or build authentication
        if auth = user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])
          auth.update_attributes(:token => oauth['credentials']['token'])
        else
          #this user is a first time authenticated object.  if this user has signed up, but never confirmed we want to override that.
          if !user.confirmed?
            user.confirm!
          end

          user.authentications.build(:uid => oauth['uid'], :provider => oauth['provider'], :token => oauth['credentials']['token'])
        end

        return user
      else
        return nil
      end
    end

  end

  #devisey place to apply our user data to a new user.
  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.facebook_data']
        user.email = params['email'] ||= data['user_info']['email']
        user.username = params['username'] ||= data['user_info']['nickname']
        user.authentications.build(:uid => data['uid'], :provider => data['provider'], :token => data['credentials']['token'])
        user.skip_confirmation!
      end
    end
  end

  def password_required?
    authentications.empty? && super
  end

  #protected

  #if I have an authentication, then do not require account confirmation.
  #def confirmation_required?
  #  false #authentications.empty? && super
  #end

  def hashed_file_name
    #"#{self.id}-#{self.avatar_file_name.gsub( /[^a-zA-Z0-9_\.]/, '_')}"
    "#{Digest::MD5.hexdigest(self.avatar_file_name)}#{File.extname(self.avatar_file_name)}"
  end
end
