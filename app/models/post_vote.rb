class PostVote < ActiveRecord::Base
  belongs_to :post
  belongs_to :user

  validate  :voter_rep, :voter_must_not_be_author, :duplicate_vote_validator

  private
    #our custom validators
    def voter_rep
      if vote == -1 && user.rep < 1
        errors.add(:user_id, 'You must have rep to vote down!')
      end
    end

    def voter_must_not_be_author
      if user_id == post.user_id
        errors.add(:user_id, 'You cannot vote on your own posts!')
      end
    end

    def duplicate_vote_validator
      count = PostVote.find_by_post_id_and_user_id(post_id, user_id)

      if count
        errors.add(:post_id, 'You cannot vote on a post twice')
      end
    end
end
