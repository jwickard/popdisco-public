class Post < ActiveRecord::Base
  belongs_to :user, :counter_cache => true
  has_and_belongs_to_many :counter_points, :class_name => 'Post', :join_table => 'counter_points', :association_foreign_key => 'counter_point_id', :foreign_key => 'post_id'
  has_many :comments
  has_many :post_votes
  acts_as_taggable

  validates_presence_of :title, :body, :user

  def up_vote_count
    PostVote.count(:conditions => "post_id = #{id} and vote = 1")
  end

  def down_vote_count
    PostVote.count(:conditions => "post_id = #{id} and vote = -1")
  end

  def user_can_edit?(user)
    !user.nil? and user.id == user_id
  end

  def user_can_post_counterpoint?(user)
    !user.nil? and user.id != user_id
  end

  def user_can_vote?(voter)
    #author cannot vote on their own post
    if voter.nil? or (voter.id == user_id)
      return false
    end

    #voter cannot vote more than once
    post_votes.each do |vote|
      if vote.user_id == voter.id
        return false
      end
    end

    true
  end
end
