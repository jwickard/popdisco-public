class CommentVote < ActiveRecord::Base
  belongs_to :comment
  belongs_to :user

  validate :voter_author_validator, :voter_rep_validator, :duplicate_vote_validator

  private
    def voter_author_validator
      if user_id == comment.user_id
        errors.add(:user_id, 'You cannot vote on your own comments!')
      end
    end

    def voter_rep_validator
      if vote == -1 && user.rep < 1
        errors.add(:user_id, 'You must have rep to vote down!')
      end
    end

    def duplicate_vote_validator
      count = CommentVote.find_by_comment_id_and_user_id(comment_id, user_id)

      if count
        errors.add(:user_id, 'You cannot vote on a post twice')
      end
    end
end
