class Affiliation < ActiveRecord::Base
  belongs_to :organization
  belongs_to :user

  validates_presence_of :user_id
  validates_uniqueness_of :user_id, :message => "already belongs to this organization",  :scope => :organization_id

  #TODO figure out why this will fail if we build an affiliation on an organization that has not been saved
  #validates_presence_of :organization_id
end
