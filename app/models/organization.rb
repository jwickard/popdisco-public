class Organization < ActiveRecord::Base
  has_many :affiliations
  has_many :members, :through => :affiliations, :class_name => 'User', :source => :user

  def user_can_edit?(user)
    if user.nil?
      false
    else
      affiliation = self.affiliations.find_by_user_id(user.id)

      (!affiliation.nil? and affiliation.admin?)
    end
  end

  def user_is_a_member?(user)
    if user.nil?
      false
    else
      self.affiliations.where(user_id: user.id).any?
    end
  end
end
