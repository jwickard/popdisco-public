require 'spec_helper'

describe Post do

  before(:each) do
    @article = Post.make!
  end

  it 'should be taggable' do
    ActsAsTaggableOn::Tag.count.should eq(0)

    expect {
      Post.make!(tag_list: 'One, Two')
    }.to change(ActsAsTaggableOn::Tag, :count).by(2)
  end

  describe 'vote counts' do
    it 'should discriminate vote counts on up or down votes' do
      @article.post_votes.create!(user: User.make!, vote: 1)
      @article.post_votes.create!(user: User.make!(rep: 1), vote: -1)

      @article.up_vote_count.should eql(1)
      @article.down_vote_count.should eql(1)
      @article.post_votes.size.should eql(2)
    end
  end



  describe 'user_can_edit?' do
    it 'should return true if the author is passed' do
      @article.user_can_edit?(@article.user).should be_true
    end

    it 'should return false if the author is not passed' do
      @article.user_can_edit?(User.make!).should be_false
    end

    it 'should return false if a nil user is passed' do
      @article.user_can_edit?(nil).should be_false
    end
  end

  describe 'user_can_vote?' do
    it 'should return true given a voter who is not the author' do
      @article.user_can_vote?(User.make!).should be_true
    end

    it 'should not allow a user to vote twice' do
      voter = User.make!
      @article.user_can_vote?(voter).should be_true

      @article.post_votes.create!(user: voter, vote: 1)

      @article.user_can_vote?(voter).should be_false
    end

    it 'should return false for nil user' do
      @article.user_can_vote?(nil).should be_false
    end

    it 'should return false if the voter is also the author' do
      @article.user_can_vote?(@article.user).should be_false
    end
  end

  describe 'user_can_post_counterpoint?' do
    it 'should return true given a user who is not the original post author' do
      @article.user_can_post_counterpoint?(User.make!).should be_true
    end

    it 'should return false for a nil user' do
      @article.user_can_post_counterpoint?(nil).should be_false
    end

    it 'should return false if user is the author of the post' do
      @article.user_can_post_counterpoint?(@article.user).should be_false
    end
  end

  describe 'validations' do
    it 'should require a title' do
      article = Post.make(title: nil)

      article.should_not be_valid
      article.errors[:title].should_not be_nil
    end

    it 'should require a body' do
      article = Post.make(body: nil)

      article.should_not be_valid
      article.errors[:body].should_not be_nil
    end

    it 'should require an author' do
      article = Post.make(user: nil)

      article.should_not be_valid
      article.errors[:user_id].should_not be_nil
    end
  end
end
