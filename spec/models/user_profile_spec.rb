require 'spec_helper'

describe UserProfile do
   it 'should belong to a user' do
    Authentication.reflect_on_association(:user).macro.should eql(:belongs_to)
  end
end
