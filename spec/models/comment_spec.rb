require 'spec_helper'

describe Comment do
  before(:each) do
    @voter = User.make!
    @voter.confirm!
    @voter2 = User.make!(rep: 1)
    @voter2.confirm!
    @article = Post.make!
    @comment = Comment.make!(post: @article)
  end

  describe 'vote counts' do
    it 'should discriminate vote counts on up or down votes' do
      @comment.comment_votes.create!(:user_id => @voter.id, :vote => 1)
      @comment.comment_votes.create!(:user_id => @voter2.id, :vote => -1)

      @comment.up_vote_count.should eql(1)
      @comment.down_vote_count.should eql(1)
      @comment.comment_votes.size.should eql(2)
    end
  end

  describe 'validations' do
    it 'should require the body not be empty' do
      comment = Comment.create(user: User.make!, post: Post.make!)
      comment.should_not be_valid
      comment.errors[:body].should_not be_nil
    end
  end
  
  describe 'user_can_vote?' do
    it 'should return true given a voter who is not the author' do
      @comment.user_can_vote?(@voter).should be_true
    end

    it 'should not allow a user to vote twice' do
      @comment.user_can_vote?(@voter).should be_true

      @comment.comment_votes.create!(:user_id => @voter.id, :vote => 1)

      @comment.user_can_vote?(@voter).should be_false
    end

    it 'should return false for nil user' do
      @comment.user_can_vote?(nil).should be_false
    end

    it 'should return false if the voter is also the author' do
      @comment.user_can_vote?(@comment.user).should be_false
    end
  end
end
