require 'spec_helper'

describe Organization do
  describe 'related models' do
    it 'should have many affiliations' do
      Organization.reflect_on_association(:affiliations).macro.should eql(:has_many)
    end

    it 'should have many members' do
      Organization.reflect_on_association(:members).macro.should eql(:has_many)
    end
  end

  describe 'user_can_edit?' do
    it 'should return false if the supplied user is nil' do
      org = Organization.make!

      org.user_can_edit?(nil).should be_false
    end

    it 'should return false if supplied user is not an organization admin' do
      org = Organization.make!

      org.user_can_edit?(User.make!).should be_false
    end

    it 'should return true if supplied user is the organizations admin' do
      user = User.make!

      org = Organization.make!

      org.affiliations.create!(user: user, admin: true)

      org.user_can_edit?(user).should be_true
    end
  end

  describe 'user_is_a_member?' do
    it 'should return false if user is nil' do
      org = Organization.make!

      org.user_is_a_member?(nil).should be_false
    end

    it 'should return false if the user has no affiliation record with an organization' do
      org = Organization.make!

      org.user_is_a_member?(User.make!).should be_false
    end

    it 'should return true if the user has an association with the organization' do
      user = User.make!

      org = Organization.make!

      org.affiliations.create!(user: user)

      org.user_is_a_member?(user).should be_true
    end
  end
end