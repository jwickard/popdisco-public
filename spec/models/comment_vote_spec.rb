require 'spec_helper'

describe CommentVote do
  before(:each) do
    @comment = Comment.make!
  end

  describe 'validations' do
    it 'should validate that the voter is not also the author' do
      vote = @comment.comment_votes.build(user: @comment.user, vote: 1)
      vote.save.should be_false

      vote.errors[:user_id].should_not be_nil
    end

    it 'should validate that a user voting down has enough rep' do
      vote = @comment.comment_votes.build(user: User.make!, vote: -1)
      vote.save.should be_false

      vote.errors[:user_id].should_not be_nil
    end

    it 'should validate that a user cannot vote on the same comment twice' do
      voter = User.make!

      @comment.comment_votes.create(user: voter, vote: 1)

      vote = @comment.comment_votes.build(user: voter, vote: 1)
      vote.save.should be_false

      vote.errors[:user_id].should_not be_nil
    end
  end
end
