require 'spec_helper'

describe PostVote do
  before(:each) do
    @article = Post.make!
  end

  describe 'validations' do
    it 'should validate that the voter is not also the author' do
      vote = @article.post_votes.build(user: @article.user, vote: 1)
      vote.save.should be_false
      vote.errors[:user_id].should_not be_nil
    end

    it 'should validate that a user voting down has enough rep' do
      vote = @article.post_votes.build(user: User.make!, vote: -1)
      vote.save.should be_false

      vote.errors[:user_id].should_not be_nil
    end

    it 'should validate that a user cannot vote on the same post twice' do
      voter = User.make!
      @article.post_votes.create(user: voter, vote: 1)

      vote = @article.post_votes.build(user: voter, vote: 1)

      vote.save.should be_false

      vote.errors[:post_id].should_not be_nil
    end
  end
end
