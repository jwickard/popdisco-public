require "spec_helper"

describe Affiliation do
  describe 'related models' do
    it 'should be belong to a user' do
      Affiliation.reflect_on_association(:user).macro.should eql(:belongs_to)
    end

    it 'should belong to an organization' do
      Affiliation.reflect_on_association(:organization).macro.should eql(:belongs_to)
    end
  end

  describe 'validations' do
    it 'should not allow a user to belong to an organization more than once' do
      organization = Organization.make!
      user = User.make!

      membership = organization.affiliations.build(user: user)

      membership.save.should be_true

      membership = organization.affiliations.build(user: user)

      membership.save.should be_false

      membership.errors[:user].should_not be_nil
    end
  end
end