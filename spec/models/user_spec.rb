require 'spec_helper'

describe User do
  def valid_oauth
    OmniAuth.config.mock_auth[:facebook]
  end

  it 'should validate uniqueness of user name' do
    user = User.make!(username: 'testguy')

    user.should be_valid
    user.errors.should be_empty

    user2 = User.make(username: 'testguy')

    user2.should_not be_valid
    user2.should have_at_least(1).errors_on(:username)
  end

  describe 'new_with_session' do
    describe 'without omniauth data' do
      it 'should create a new user from passed params' do
        user = User.new_with_session({'email' => 'joeuser@example.com', 'username' => 'joe.user'}, {})

        user.email.should eql('joeuser@example.com')
        user.username.should eql('joe.user')
      end

      it 'should not have an authentication' do
        user = User.new_with_session({'email' => 'joeuser@example.com', 'username' => 'joe.user'}, {})

        user.authentications.size.should be(0)
      end
    end

    describe 'with facebook auth' do
      it 'should build an authentication object' do
        user = User.new_with_session({'email' => 'joeuser@example.com', 'username' => 'joe.user'}, {'devise.facebook_data' => valid_oauth})

        user.authentications.size.should be(1)

        auth = user.authentications[0]

        auth.should_not be_nil
        auth.provider.should eql(valid_oauth['provider'])
        auth.uid.should eql(valid_oauth['uid'])
        auth.token.should eql(valid_oauth['credentials']['token'])
      end

      it 'should confirm the new user' do
        user = User.new_with_session({'email' => 'joeuser@example.com', 'username' => 'joe.user'}, {'devise.facebook_data' => valid_oauth})

        user.confirmed?.should be_true
      end

      it 'should default email to omniauth supplied email' do
        user = User.new_with_session({}, {'devise.facebook_data' => valid_oauth})

        user.email.should eql(valid_oauth['user_info']['email'])
      end

      it 'should default email to omniauth supplied email' do
        user = User.new_with_session({}, {'devise.facebook_data' => valid_oauth})

        user.username.should eql(valid_oauth['user_info']['nickname'])
      end
    end
  end

  describe 'find_for_facebook_oauth' do

    describe 'without signed in user' do
      it 'should return nil if authentication does not match passed oauth' do
        User.find_for_facebook_oauth(valid_oauth).should be_nil
      end

      it 'should not return a readonly record' do
        user = User.new({:username => 'user.one', :email => 'user@example.com'})
        user.confirm!

        found = User.find_for_facebook_oauth(valid_oauth)

        found.readonly?.should be_false
      end

      it 'should return existing user for matching authentication' do
        user = User.new({:username => 'user.one', :email => 'user@example.com'})
        user.authentications.build({:uid => valid_oauth['uid'], :provider => valid_oauth['provider'], :token => 'tokenshouldchange'})
        user.confirm!

        found = User.find_for_facebook_oauth(valid_oauth)

        found.should eql(user)
      end

      it 'should update token when matching authentication exists' do
        user = User.new({:username => 'user.one', :email => 'user@example.com'})
        user.authentications.build({:uid => valid_oauth['uid'], :provider => valid_oauth['provider'], :token => 'tokenshouldchange'})
        user.confirm!

        user = User.find_for_facebook_oauth(valid_oauth)

        authentication = user.authentications.first

        authentication.token.should eql(valid_oauth['credentials']['token'])
      end

      it 'should find a verified user by email if no authentication exists' do
        user = User.new({:username => 'user.one', :email => 'user@example.com'})
        user.confirm!

        found = User.find_for_facebook_oauth(valid_oauth)

        found.should eql(user)
      end

      it 'should find a user only by authentication if verified but email does not match' do
        user = User.new({:username => 'user.one', :email => 'nonmatching@email.com'})
        user.authentications.build({:uid => valid_oauth['uid'], :provider => valid_oauth['provider'], :token => 'tokenshouldchange'})
        user.confirm!

        found = User.find_for_facebook_oauth(valid_oauth)

        found.should eql(user)
      end

      it 'should find a user only by authentication if user is not verified by facebook' do
        user = User.new({:username => 'user.one', :email => 'nonmatching@email.com'})
        user.authentications.build({:uid => valid_oauth['uid'], :provider => valid_oauth['provider'], :token => 'tokenshouldchange'})
        user.confirm!

        valid_oauth['extra']['user_hash']['verified'] = false

        found = User.find_for_facebook_oauth(valid_oauth)

        found.email.should eql(user.email)

        valid_oauth['extra']['user_hash']['verified'] = true
      end

      it 'should confirm an existing user if that user was unconfirmed' do
        user = User.create!({:username => 'user.one', :email => 'user@example.com', :password => 'smokey', :password_confirmation => 'smokey'})
        #user.save
        #user.save(:validate => false)

        user.confirmed_at.should be_nil

        found = User.find_for_facebook_oauth(valid_oauth)

        #should now be confirmed because we are going to authenticate the user anyway.
        found.confirmed_at.should_not be_nil

      end
    end

    describe 'with signed in user' do
      it 'should update user email, if user signed in and email is nil' do
        user_without_email = User.new({:username => 'user.one'})
        user_without_email.confirm!

        user_without_email.email.should eql('')

        user = User.find_for_facebook_oauth(valid_oauth, user_without_email)

        user.email.should eql(valid_oauth['user_info']['email'])
      end

      it 'should add a new authentication if one does not exist' do
        user = User.new({:username => 'user.one', :email => 'user@example.com'})
        user.confirm!

        #we don't have any authentications
        user.authentications.empty?.should be_true

        user = User.find_for_facebook_oauth(valid_oauth, user)

        authentication = user.authentications.first

        authentication.uid.should eql(valid_oauth['uid'])
        authentication.provider.should eql(valid_oauth['provider'])
        authentication.token.should eql(valid_oauth['credentials']['token'])

        authentication.persisted?.should be_false
      end

      it 'should update the token if authentication exists' do
        user = User.new({:username => 'user.one', :email => 'user@example.com'})
        user.authentications.build({:uid => valid_oauth['uid'], :provider => valid_oauth['provider'], :token => 'tokenshouldchange'})
        user.confirm!

        user = User.find_for_facebook_oauth(valid_oauth, user)

        authentication = user.authentications.first

        authentication.token.should eql(valid_oauth['credentials']['token'])
      end
    end
  end
end
