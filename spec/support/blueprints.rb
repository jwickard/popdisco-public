require 'machinist/active_record'

User.blueprint do
  username { "user#{sn}" }
  email { "user#{sn}@example.com" }
  password { "password" }
end

UserProfile.blueprint do
  user
  name { "First#{sn} Last#{sn}"}
  website { "http://www.user.com" }
  location { "Minnesota" }
  birthday { 33.years.ago }
  bio { "User #{sn} bio" }
end

Achievement.blueprint do
  slug { "ach#{sn}" }
  name { "Achievement #{sn}" }
  rep_bonus { 2000 }
end

Post.blueprint do
  user
  title { "Post #{sn}" }
  body { "Post Body for #{sn}" }
end

Comment.blueprint do
  user
  post
  body { "Comment Body #{sn}" }
end

Organization.blueprint do
  name { "Organization #{sn}" }
  description { "Description for Organization #{sn}" }
  enabled { true }
end
