module ViewMacros


  def setup_anonymous_user
    before(:each) do
      warden = mock('Warden')
      warden.stub!(:authenticate).and_return nil
      @request.env['warden'] = warden
      @request.env["devise.mapping"] = Devise.mappings[:user]
    end
  end

  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = User.create(:username => 'joeuser', :email => 'joe@user.com', :rep => 10)
      user.confirm!
      sign_in user
      @current_user = user
    end
  end
end