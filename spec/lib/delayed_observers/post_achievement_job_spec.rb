require 'spec_helper'

describe PostAchievementJob do
  before(:each) do
    @user = User.make!
    @user.confirm!

    #TODO figure out why we can't machine an achievement
    @achievement = Achievement.create!(name: 'Frequent Flyer', slug: 'freq-fly', rep_bonus: 2000)

    Post.make!(5, user: @user)

    @post = Post.last
  end

  it 'should create a new achievement if the user has posted 5 times' do

    job = PostAchievementJob.new(@post)

    job.perform

    @user.achievements.should include(@achievement)

  end

  it 'should change author rep by achievement rep mod amount' do
    @post.user.rep.should eql(0)

    job = PostAchievementJob.new(@post)
    job.perform

    @post.user.rep.should eq(@achievement.rep_bonus)

  end

  it 'should create a new achievement only if achievement does not already exist' do
    @user.achievements << @achievement

    job = PostAchievementJob.new(@post)
    job.perform

    #we should only see the achievement once.
    @post.user.achievements.where(:slug => @achievement.slug).count.should eql(1)

    #this is cheap, but it makes sure that the job was skipped
    @post.user.rep.should eq(0)
  end
end