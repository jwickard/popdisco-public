require 'spec_helper'

describe PostAchievementObserver do
  it 'should queue up a PostAchevementJob' do
    Delayed::Job.count.should eq(0)
    post = Post.create!({title: 'The Title', body: 'the body', user: mock_model(User)})
    Delayed::Job.count.should eq(1)
    Delayed::Job.first.handler.should eq(PostAchievementJob.new(post).to_yaml)
  end
end
