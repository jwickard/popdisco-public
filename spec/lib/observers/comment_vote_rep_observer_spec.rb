require 'spec_helper'

describe CommentVoteRepObserver do
  before(:each) do
    @comment = Comment.make!
  end

  describe 'vote_up' do
    it 'should add rep to the author' do
      @comment.comment_votes.create!(user: User.make!, vote: 1)

      #should increase the authors rep.
      @comment.post.user.rep.should eql(1)
    end
  end

  describe 'vote_down' do

    it 'should remove rep from the author' do
      #voter must have positive rep:
      @comment.comment_votes.create!(user: User.make!(rep: 1), vote: -1)

      #should decrease the authors rep.
      @comment.post.user.rep.should eql(-1)
    end

    it 'should remove rep from the voter' do
      #voter must have positive rep:
      vote = @comment.comment_votes.create!(user: User.make!(rep: 1), vote: -1)

      #should increase the authors rep.
      vote.user.rep.should eql(0)
    end
  end

  describe 'cancel save' do
    it 'should return false if there is a problem saving a user' do
      #short circuit the author
      User.any_instance.should_receive(:save).and_return(false)

      vote = @comment.comment_votes.build(user: User.make!, vote: 1)
      vote.save.should be_false
    end

    it 'should return false if there is a problem decrementing the voter' do
      voter = mock_model(User, :rep => 5)
      voter.should_receive(:rep=).with(4).and_return(nil)
      voter.should_receive(:save).and_return(false)

      vote = @comment.comment_votes.build(user: voter, vote: -1)

      vote.save.should be_false
    end
  end
end