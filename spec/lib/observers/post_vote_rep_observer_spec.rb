require 'spec_helper'

describe PostVoteRepObserver do
  before(:each) do
    @author = User.make!
    @author.confirm!
    @voter = User.make!
    @voter.confirm!
    @article = Post.make!(user: @author)
    @author.organizations << Organization.make!
  end

  describe 'positive vote' do

    it 'should increment the articles vote_counter_cache' do
      vote = @article.post_votes.create(user: @voter, vote: 1)

      vote.post.vote_count_cache.should eql(1)
    end

    it 'should add rep to the author' do
      vote = @article.post_votes.create!(user: @voter, vote: 1)

      #should increase the authors rep.
      vote.post.user.rep.should eql(1)
    end

    it 'should add rep to the authors organizations' do
      @article.post_votes.create(user: @voter, vote: 1)

      @author.organizations.first.rep.should eql(1)
    end
  end

  describe 'negative vote' do

    it 'should decrement the post vote_counter_cache' do
      #voter must have positive rep:
      vote = @article.post_votes.create(user: User.make!(rep: 1), vote: -1)

      vote.post.vote_count_cache.should eql(-1)
    end

    it 'should remove rep from the author' do
      #voter must have positive rep:
      vote = @article.post_votes.create(user: User.make!(rep: 1), vote: -1)

      #should decrease the authors rep.
      vote.post.user.rep.should eql(-1)
    end

    it 'should remove rep from the authors organizations' do
      #voter must have positive rep:
      @article.post_votes.create(user: User.make!(rep: 1), vote: -1)

      #should decrease authors organization rep.
      @author.organizations.first.rep.should eql(-1)
    end

    it 'should remove rep from the voter' do
      #voter must have positive rep:
      vote = @article.post_votes.create(user: User.make!(rep: 1), vote: -1)

      #should decrease the authors rep.
      vote.user.rep.should eql(0)
    end
  end

  describe 'observer save errors' do
    it 'should return false on author save failure' do
      User.any_instance.should_receive(:save).and_return(false)

      vote = @article.post_votes.build(user: @voter, vote: 1)

      vote.save.should be_false
    end

    it 'should return false on voter save failure' do
      voter = mock_model(User, :rep => 5)
      voter.should_receive(:rep=).with(4).and_return(nil)
      voter.should_receive(:save).and_return(false)

      vote = @article.post_votes.build(user: voter, vote: -1)

      vote.save.should be_false
    end

    it 'should return false on post update failure' do
      observer = PostVoteRepObserver.instance

      post = mock_model(Post, title: 'The Title', body: 'The Body', vote_count_cache: 3)
      post.should_receive(:update_attributes).and_return false

      vote = mock_model(PostVote, post: post, vote: 1)

      observer.before_save(vote).should be_false
    end

    it 'should return false on organization save failure' do
      observer = PostVoteRepObserver.instance

      org = mock_model(Organization, rep: 2)
      org.should_receive(:rep=)
      org.should_receive(:save).and_return(false)

      @author.stub!(:organizations).and_return([org])

      post = mock_model(Post, user: @author, title: 'The Title', body: 'The Body', vote_count_cache: 3)
      post.should_receive(:update_attributes).and_return(true)

      vote = mock_model(PostVote, post: post, vote: 1)

      observer.before_save(vote).should be_false
    end
  end
end
