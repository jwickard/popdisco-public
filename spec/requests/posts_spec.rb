require 'spec_helper'

describe 'Index Page' do
  describe 'GET /' do
    it 'allows a user to click on post title to view article' do
      article = Post.make!

      visit root_path

      page.status_code.should be(200)

      click_link article.title

      page.status_code.should be(200)

      current_path.should eql(post_path(article))

    end

    it 'allows a user to click the logo to go to portal index' do
      #click on sign in.
      visit new_user_session_path

      page.status_code.should be(200)

      #click the log to be taken back to the root path.
      click_link 'logo-home-link'

      #should be success
      page.status_code.should be(200)

      #we should be taken home.
      current_path.should eql(root_path)
    end
  end
end