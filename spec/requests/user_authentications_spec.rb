require 'spec_helper'

describe "UserAuthentications" do

  before(:each) do
    ActionMailer::Base.deliveries = []
  end

  describe "User Sign Up" do
    let(:user) { mock_model(User, :username => 'jwickard', :email => 'jwickard@gmail.com', :password => 'supersecret')}

    #it "should allow user to sign up" do
    #  visit new_user_session_path
    #
    #  click_link 'Sign up'
    #  fill_in 'Username', :with => user.username
    #  fill_in 'Email', :with => user.email
    #  fill_in 'Password', :with => user.password
    #  fill_in 'Password confirmation', :with => user.password
    #  click_button 'Sign up'
    #
    #  current_path.should eql(root_path)
    #
    #  created_user = User.find_by_email(user.email)
    #
    #  created_user.should_not be_nil
    #  created_user.email.should eql(user.email)
    #
    #  page.should have_content("You have signed up successfully. However, we could not sign you in because your account is unconfirmed.")
    #  current_path.should eql(root_path)
    #
    #  mail = ActionMailer::Base.deliveries.last
    #  mail.to.should include(created_user.email)
    #  #TODO figure out why using the url won't work
    #  mail.body.should include(user_confirmation_path(:confirmation_token => created_user.confirmation_token))
    #
    #  #user has signed up but is unconfirmed
    #  created_user.confirmed_at.should be_nil
    #end

    #it 'should allow user to confirm their account' do
    #  existing = User.create(:email => 'nobody@nowhere.com', :username => 'nobody', :password => 'supersecret', :password_confirmation => 'supersecret')
    #  #empty out the email we just sent.
    #  ActionMailer::Base.deliveries = []
    #  #confirm we have no email sent
    #  ActionMailer::Base.deliveries.size.should eql(0)
    #
    #  #lets confirm our user.
    #  visit user_confirmation_path :confirmation_token => existing.confirmation_token
    #
    #  confirmed_user = User.find_by_email(existing.email)
    #  confirmed_user.confirmed_at.should_not be_nil
    #
    #  #it should sign us in, redirect us to the root path and set a success message.
    #  current_path.should eql(root_path)
    #  page.should have_content("Your account was successfully confirmed. You are now signed in.")
    #
    #end

    it 'should resend a users confirmation message on demand' do
      existing = User.make!

      #empty out the email we just sent.
      ActionMailer::Base.deliveries = []

      visit new_user_confirmation_path

      #confirm we have no email sent
      ActionMailer::Base.deliveries.size.should eql(0)

      fill_in 'Email', :with => existing.email
      click_button 'Resend confirmation instructions'

      mail = ActionMailer::Base.deliveries.last
      mail.to.should include(existing.email)
      mail.body.should include(user_confirmation_path(:confirmation_token => existing.confirmation_token))

      current_path.should eql(new_user_session_path)
      page.should have_content('You will receive an email with instructions about how to confirm your account in a few minutes.')

    end

    it 'should allow user to reset their password' do
      existing = User.make!
      existing.confirm!

      existing.reset_password_sent_at.should be_nil

      visit new_user_password_path

      fill_in 'Email', :with => existing.email
      click_button 'Send me reset password instructions'

      #freshen our user data
      existing = User.find_by_email(existing.email)

      mail = ActionMailer::Base.deliveries.last
      mail.to.should include(existing.email)
      mail.body.should include(edit_user_password_path(:reset_password_token => existing.reset_password_token))

      existing.reset_password_sent_at.should_not be_nil

      current_path.should eql(new_user_session_path)
      page.should have_content('You will receive an email with instructions about how to reset your password in a few minutes.')

      visit edit_user_password_path(:reset_password_token => existing.reset_password_token)

      fill_in 'New password', :with => 'supersonic'
      fill_in 'Confirm new password', :with => 'supersonic'
      click_button 'Change my password'

      page.should have_content('Your password was changed successfully. You are now signed in.')

      current_path.should eql(root_path)
    end
  end
end
