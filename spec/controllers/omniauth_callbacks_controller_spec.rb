require 'spec_helper'

describe OmniauthCallbacksController do

  before do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
  end

  describe 'existing user' do
    it 'should sign in user' do
      user = User.make!
      user.confirm!
      User.should_receive(:find_for_facebook_oauth).with(OmniAuth.config.mock_auth[:facebook], nil).and_return(user)

      get 'facebook'

      warden.authenticated?(:user).should be_true
    end

    it 'should redirect back or to root url' do
      user = User.make!
      user.confirm!
      User.should_receive(:find_for_facebook_oauth).with(OmniAuth.config.mock_auth[:facebook], nil).and_return(user)

      get 'facebook'

      response.should redirect_to(root_path)
    end
  end

  describe 'with non-existing user' do
    it 'should set oauth data in session scope' do
      session['devise.facebook_data'].should be_nil

      get 'facebook'

      session['devise.facebook_data'].should eql(OmniAuth.config.mock_auth[:facebook])
    end

    it 'should redirect user to registration page' do
      get 'facebook'

      response.should redirect_to(new_user_registration_path)
    end
  end
end
