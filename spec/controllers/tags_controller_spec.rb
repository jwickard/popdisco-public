require 'spec_helper'

describe TagsController do
  login_user

  describe "GET 'index'" do
    it "returns http success" do
      get :index
      response.should be_success
    end

    it 'should assign a topics list' do
      get :index

      assigns(:topics).should_not be_nil
    end

    it 'should assign a topics header' do
      get :index

      assigns(:sort_title).should_not be_nil
    end
  end

  describe "GET posts_by_tag" do
    it 'should show only posts associated with a supplied tag' do
      one = Post.make!(tag_list: 'One, Two')
      two = Post.make!(tag_list: 'Two')
      three = Post.make!(tag_list: 'Three')


      get :posts_by_tag, :tag => 'Two'

      assigns(:posts).should include(one)
      assigns(:posts).should include(two)
      assigns(:posts).should_not include(three)
    end

    it 'should render index on success' do
      Post.make!(tag_list: 'One, Two')

      get :posts_by_tag, :tag => 'Two'

      response.should render_template('posts_by_tag')
    end
  end

  describe "GET recent_tags" do
    it 'should assign tag list' do
      Post.make!(tag_list: 'One, Two')

      get :recent_tags

      assigns(:topics).should_not be_nil
    end

    it 'should validate that only recent tags were loaded' do
      post = Post.make!(tag_list: 'one, two')

      #update the first tag to have been created 2 days go.
      one = ActsAsTaggableOn::Tag.find_by_name('one')
      tagging = ActsAsTaggableOn::Tagging.find_by_tag_id_and_taggable_id(one.id, post.id)
      tagging.created_at = 2.days.ago
      tagging.save!

      get :recent_tags

      assigns(:topics).should_not include(one)

    end

    it 'should render the index' do
      Post.make!(tag_list: 'one, two')

      get :recent_tags

      response.should render_template('index')
    end

    it 'should assign a topics header' do
      get :recent_tags

      assigns(:sort_title).should_not be_nil
    end
  end

  describe "GET popular_tags" do
    it 'should render the index' do
      get :popular_tags

      response.should render_template('index')
    end

    it 'should assign a topics list' do
      Post.make!(tag_list: 'One, Two')

      get :popular_tags

      assigns(:topics).should_not be_nil
    end

    it 'should order topic by count then name' do
      Post.make!(tag_list: 'a, b, c')
      Post.make!(tag_list: 'bb')
      Post.make!(tag_list: 'b, bb, c')
      Post.make!(tag_list: 'c')

      a = ActsAsTaggableOn::Tag.find_by_name('a')
      b = ActsAsTaggableOn::Tag.find_by_name('b')
      bb = ActsAsTaggableOn::Tag.find_by_name('bb')
      c = ActsAsTaggableOn::Tag.find_by_name('c')

      get :popular_tags

      #first is c, because it was tagged 3 total times.
      assigns(:topics).first.should eq(c)
      #next is b because secondary sort is by name and b && bb have two votes
      assigns(:topics)[1].should eq(b)
      assigns(:topics)[2].should eq(bb)
      #a is last because it was tagged once
      assigns(:topics).last.should eq(a)

    end

    it 'should assign a topics header' do
      get :popular_tags

      assigns(:sort_title).should_not be_nil
    end
  end
end
