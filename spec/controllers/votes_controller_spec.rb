require 'spec_helper'

describe VotesController do
  login_user

  let(:the_post) { Post.make! }
  let(:the_comment) { Comment.make!(post: the_post) }


  describe "vote_post_up" do
    it 'will assign success message and redirect to post on success' do

      #vote!!
      post :vote_post_up, :post_id => the_post.id

      #we succeeded so we set a success message
      flash[:notice].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end

    it 'will assign alert message and redirect to post if failed' do
      #failed!
      PostVote.any_instance.should_receive(:save).and_return(false)

      #vote!!
      post :vote_post_up, :post_id => the_post.id

      #we failed so we set an alert message
      flash[:alert].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end
  end

  describe "vote_post_down" do
    it 'will assign success message and redirect to post on success' do

      #vote!!
      post :vote_post_down, :post_id => the_post.id

      #we succeeded so we set a success message
      flash[:notice].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end

    it 'will assign alert message and redirect to post if failed' do
      #failed!
      PostVote.any_instance.should_receive(:save).and_return(false)

      #vote!!
      post :vote_post_down, :post_id => the_post.id

      #we failed so we set an alert message
      flash[:alert].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end
  end

  describe "vote_comment_up" do
     it 'will assign success message and redirect to post on success' do

      #vote!!
      post :vote_comment_up, :comment_id => the_comment.id

      #we succeeded so we set a success message
      flash[:notice].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end

    it 'will assign alert message and redirect to post if failed' do
      #failed!
      CommentVote.any_instance.should_receive(:save).and_return(false)

      #vote!!
      post :vote_comment_up, :comment_id => the_comment.id

      #we failed so we set an alert message
      flash[:alert].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end
  end

  describe "Post vote_comment_down" do
     it 'will assign success message and redirect to post on success' do

      #vote!!
      post :vote_comment_down, :comment_id => the_comment.id

      #we succeeded so we set a success message
      flash[:notice].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end

    it 'will assign alert message and redirect to post if failed' do
      #failed!
      CommentVote.any_instance.should_receive(:save).and_return(false)

      #vote!!
      post :vote_comment_down, :comment_id => the_comment.id

      #we failed so we set an alert message
      flash[:alert].should_not be_nil

      response.should redirect_to(post_url(the_post))
    end
  end
end
