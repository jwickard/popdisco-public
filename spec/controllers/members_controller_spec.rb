require 'spec_helper'

describe MembersController do

  describe "GET 'index'" do
    it "should assign a list of members" do
      user = User.make!
      user.confirm!

      get :index
      response.should be_success
      assigns[:members].size.should eql(1)
      assigns[:members].should include(user)
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      user = User.make!
      user.confirm!

      get :show, :id => user.id
      response.should be_success

      assigns[:member].should eql(user)
    end
  end

  describe 'GET recent_members' do
    it 'should assign a list of members' do
      get :recent_members

      assigns[:members].should_not be_nil
    end

    it 'should render the index template' do
      get :recent_members

      response.should render_template(:index)
    end

    it 'should sort members by created_at, username' do
      now = Time.now
      fourth = User.make!(created_at: now - 4.day)
      fourth.confirm!
      third = User.make!(username: 'third.user', created_at: now - 3.day)
      third.confirm!
      second = User.make!(username: 'second.user', created_at: now - 3.day)
      second.confirm!
      first = User.make!
      first.confirm!

      get :recent_members

      assigns[:members].first.should eq(first)
      assigns[:members][1].should eq(second)
      assigns[:members][2].should eq(third)
      assigns[:members].last.should eq(fourth)
    end
  end

  describe 'GET users_by_reputation' do
    it 'should assign a list of members' do
      get :members_by_reputation

      assigns[:members].should_not be_nil
    end
    it 'should render the index template' do
      get :members_by_reputation

      response.should render_template :index
    end

    it 'should validate that the sort order is by reputation' do
      second = User.make!(rep: 1)
      second.confirm!
      first = User.make!(rep: 2)
      first.confirm!

      get :members_by_reputation

      assigns[:members].first.should eq(first)
      assigns[:members].last.should eq(second)
    end
  end

end
