require 'spec_helper'

describe PostsController do

  def update_values
    { title: 'Updated Title', body: 'Updated Body'}
  end

  describe 'Unauthenticated Session' do
    describe "GET index" do
      it "assigns all posts as @posts" do
        the_post = Post.make!
        get :index
        assigns(:posts).should eq([the_post])
      end

      it 'should sort most recent by vote, then post date' do
        fourth = Post.make!(created_at: 2.days.ago)
        third = Post.make!(vote_count_cache: 4, created_at: 1.day.ago)
        second = Post.make!(vote_count_cache: 5, created_at: 1.day.ago)
        first = Post.make!

        get :index

        assigns(:posts).first.should eq(first)
        assigns(:posts)[1].should eq(second)
        assigns(:posts)[2].should eq(third)
        assigns(:posts).last.should eq(fourth)
      end
    end

    describe 'GET popular_posts' do
      it 'assigns popular posts as @posts' do
        Post.make!
        get :popular_posts

        assigns(:posts).should_not be_nil
      end

      it 'should render the index template' do
        Post.make!
        get :popular_posts

        response.should render_template(:index)
      end
    end

    describe "GET show" do
      it "assigns the requested post as @post" do
        post = Post.make!
        get :show, :id => post.id
        assigns(:post).should eq(post)
        assigns(:post).view_count.should eq(1)
        assigns(:current_user).should eq(@current_user)
      end
    end

    describe 'GET new' do
      it 'should route us to login' do
        get :new
        response.should redirect_to new_user_session_path
      end
    end

    describe 'POST create' do
      it 'should redirect to login' do
        post :create, :post => update_values
        response.should redirect_to new_user_session_path
      end
    end

    describe 'PUT update' do
      it 'should redirect to login' do
        post = Post.make!
        put :update, :id => post.id, :post => update_values
        response.should redirect_to new_user_session_path
      end
    end

    describe 'DELETE destroy' do
      it 'should redirect to login' do
        post = Post.make!
        delete :destroy, :id => post.id
        response.should redirect_to new_user_session_path
      end
    end

    describe 'GET new_counterpoint' do
      it 'should redirect to login' do
        post = Post.make!
        get :new_counterpoint, :id => post.id
        response.should redirect_to new_user_session_path
      end
    end
  end

  describe "Authenticated Session" do

    login_user

    describe "GET new" do

      it "assigns a new post as @post" do
        get :new
        assigns(:post).should be_a_new(Post)
      end
    end

    describe "GET edit" do
      it "assigns the requested post as @post" do
        post = Post.make!(user: @current_user)
        get :edit, :id => post.id
        assigns(:post).should eq(post)
      end

      it "redirects to post with error if editor is not the author" do
        post = Post.make!

        get :edit, id: post.id

        response.should redirect_to post

        flash[:alert].should_not be_nil
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "creates a new Post" do
          expect {
            post :create, :post => update_values
          }.to change(Post, :count).by(1)
        end

        it "assigns a newly created post as @post" do
          post :create, :post => update_values
          assigns(:post).should be_a(Post)
          assigns(:post).should be_persisted
        end

        it "redirects to the created post" do
          post :create, :post => update_values
          response.should redirect_to(Post.last)
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved post as @post" do
          # Trigger the behavior that occurs when invalid params are submitted
          Post.any_instance.stub(:save).and_return(false)
          post :create, :post => {}
          assigns(:post).should be_a_new(Post)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          Post.any_instance.stub(:save).and_return(false)
          post :create, :post => {}
          response.should render_template("new")
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "updates the requested post" do
          post = Post.make!(user: @current_user)
          # Assuming there are no other posts in the database, this
          # specifies that the Post created on the previous line
          # receives the :update_attributes message with whatever params are
          # submitted in the request.
          Post.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
          put :update, :id => post.id, :post => {'these' => 'params'}
        end

        it "assigns the requested post as @post" do
          post = Post.make!(user: @current_user)
          put :update, :id => post.id, :post => update_values
          assigns(:post).should eq(post)
        end

        it "redirects to the post" do
          post = Post.make!(user: @current_user)
          put :update, :id => post.id, :post => update_values
          response.should redirect_to(post)
        end

        it "redirects with error if the editor is not the author" do
          post = Post.make!

          put :update, id: post.id, post: update_values

          response.should redirect_to post

          flash[:alert].should_not be_nil
        end
      end

      describe "with invalid params" do
        it "assigns the post as @post" do
          post = Post.make!(user: @current_user)
          # Trigger the behavior that occurs when invalid params are submitted
          Post.any_instance.stub(:save).and_return(false)
          put :update, :id => post.id, :post => {}
          assigns(:post).should eq(post)
        end

        it "re-renders the 'edit' template" do
          post = Post.make!(user: @current_user)
          # Trigger the behavior that occurs when invalid params are submitted
          Post.any_instance.stub(:save).and_return(false)
          put :update, :id => post.id, :post => {}
          response.should render_template("edit")
        end
      end
    end

    describe "DELETE destroy" do
      it "destroys the requested post" do
        post = Post.make!
        expect {
          delete :destroy, :id => post.id
        }.to change(Post, :count).by(-1)
      end

      it "redirects to the posts list" do
        post = Post.make!
        delete :destroy, :id => post.id
        response.should redirect_to(posts_url)
      end
    end

    describe 'GET new_counterpoint' do
      it 'assigns a new Post object' do
        post = Post.make!
        get :new_counterpoint, :id => post.id
        assigns(:post).should be_a_new(Post)
      end

      it 'assigns the counter point id.' do
        post = Post.make!
        get :new_counterpoint, :id => post.id
        assigns(:parent_id).should eql(post.id.to_s)
      end

      it 'redirects to post if current_user is author of original post' do
        post = Post.make!(user: @current_user)

        get :new_counterpoint, id: post.id

        response.should redirect_to post

        flash[:alert].should_not be_nil
      end
    end

    describe "POST create_counterpoint" do
      describe "with valid params" do
        it "creates a new counterpoint" do
          original = Post.make!
          expect {
            post :create_counterpoint, :post => update_values, :id => original.id
          }.to change(Post, :count).by(1)
        end

        it "assigns a newly created post as @post" do
          original = Post.make!
          post :create_counterpoint, :post => update_values, :id => original.id
          assigns(:post).should be_a(Post)
          assigns(:post).should be_persisted
        end

        it "is a counter point to the original" do
          original = Post.make!
          post :create_counterpoint, :post => update_values, :id => original.id
          original.counter_points.should include(Post.last)
        end

        it "has the original as a counter point" do
          original = Post.make!
          post :create_counterpoint, :post => update_values, :id => original.id
          #refresh point
          Post.last.counter_points.should include(original)
        end

        it "redirects to the created post" do
          original = Post.make!
          post :create_counterpoint, :post => update_values, :id => original.id
          response.should redirect_to(Post.last)
        end

        it 'redirects to post if current_user is author of original post' do
          post = Post.make!(user: @current_user)

          get :new_counterpoint, id: post.id

          response.should redirect_to post

          flash[:alert].should_not be_nil
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved post as @post" do
          # Trigger the behavior that occurs when invalid params are submitted
          original = Post.make!
          Post.any_instance.stub(:save).and_return(false)
          post :create_counterpoint, :post => {}, :id => original.id
          assigns(:post).should be_a_new(Post)
          assigns(:parent_id).should eql(original.id)
        end

        it "re-renders the 'new' template" do
          # Trigger the behavior that occurs when invalid params are submitted
          original = Post.make!
          Post.any_instance.stub(:save).and_return(false)
          post :create_counterpoint, :post => {}, :id => original.id
          response.should render_template("new_counterpoint")
        end
      end
    end
  end
end
