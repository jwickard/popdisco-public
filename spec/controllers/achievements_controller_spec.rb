require 'spec_helper'

describe AchievementsController do

  describe "GET 'index'" do
    it "returns http success" do
      Achievement.make!

      get :index
      response.should be_success
    end

    it 'should assign locked and unlocked achievements' do
      first_unlocked = Achievement.make!
      first_unlocked.users << User.make!
      second_unlocked = Achievement.make!
      second_unlocked.users << User.make!
      locked = Achievement.make!

      get :index

      assigns(:unlocked_achievements).should include(first_unlocked, second_unlocked)
      assigns(:unlocked_achievements).should_not include(locked)
      assigns(:locked_achievements).should include(locked)
      assigns(:locked_achievements).should_not include(first_unlocked, second_unlocked)
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      achievement = Achievement.make!

      get 'show', :id => achievement.id

      response.should be_success

      assigns(:achievement).should eq(achievement)
    end
  end

end
