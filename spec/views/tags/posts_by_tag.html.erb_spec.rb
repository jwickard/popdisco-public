require 'spec_helper'

describe "tags/posts_by_tag.html.erb" do
  before(:each) do
    user = mock_model(User, id: 2, username: 'jwickard', rep: 4)
    @posts = [
      stub_model(Post,
        body: "The Body",
        user_id: 1,
        user: user,
        created_at: Date.new
      ),
      stub_model(Post,
        body: "The Body",
        user_id: 1,
        user: user,
        created_at: Date.new
      )
    ]

    @posts.stub!(:current_page).and_return(1)
    @posts.stub!(:num_pages).and_return(1)
    @posts.stub!(:limit_value).and_return(1)
  end

  it 'should render' do
    render
  end
end