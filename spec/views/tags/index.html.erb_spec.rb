require 'spec_helper'

describe "tags/index.html.erb" do
  before(:each) do
    @topics = [ mock('Tag', count: 3, name: 'Some Tag') ]

    @topics.stub!(:current_page).and_return(1)
    @topics.stub!(:num_pages).and_return(1)
    @topics.stub!(:limit_value).and_return(1)
  end

  it 'should render successfully' do
    render
  end
end
