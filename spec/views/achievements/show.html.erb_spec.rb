require 'spec_helper'

describe "achievements/show.html.erb" do
  before(:each) do
    user =  User.make!
    assign(:achievement, mock_model(Achievement,
                                  :name => 'Frequent Flier',
                                  :slug => 'freq-fly',
                                  :description => 'Post at least 5 times!',
                                  :users => [user]))
  end

  it "renders the achievement" do
    render
  end
end
