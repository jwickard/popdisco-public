require 'spec_helper'

describe "user_profiles/new.html.erb" do
  login_user

  before(:each) do
    assign(:user_profile, stub_model(UserProfile,
      :name => "MyString",
      :website => "MyString",
      :bio => "MyText"
    ).as_new_record)
  end

  it "renders new user_profile form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => user_profiles_path, :method => "post" do
      assert_select "input#user_profile_name", :name => "user_profile[name]"
      assert_select "input#user_profile_website", :name => "user_profile[website]"
      assert_select "textarea#user_profile_bio", :name => "user_profile[bio]"
    end
  end
end
