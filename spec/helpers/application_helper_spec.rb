require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the PostsHelper. For example:
#
# describe PostsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe ApplicationHelper do
  it 'lists the most recent Tags' do
    Post.make!(tag_list: 'One, Two')

    helper.recent_tags.length.should eql(2)
  end
end
