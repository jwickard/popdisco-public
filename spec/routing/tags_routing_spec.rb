require "spec_helper"

describe TagsController do
  describe "routing" do
    it "routes to #index" do
      get("/topics").should route_to("tags#index")
    end

    it 'routes to #posts_by_tag' do
      get('/topics/reagan/posts').should route_to('tags#posts_by_tag', :tag => 'reagan')
    end

    it 'routes to #recent_tags' do
      get('/topics/recent').should route_to('tags#recent_tags')
    end

    it 'routes to #all_tags' do
      get('/topics/popular').should route_to('tags#popular_tags')
    end
  end
end
