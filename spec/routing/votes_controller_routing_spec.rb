require "spec_helper"

describe VotesController do
  describe "routing" do

    it "routes to #vote_post_up" do
      get("/vote/post/1/up").should route_to("votes#vote_post_up", :post_id => '1')
    end

    it "routes to #vote_post_down" do
      get("/vote/post/1/down").should route_to("votes#vote_post_down", :post_id => '1')
    end

    it "routes to #vote_comment_up" do
      get("/vote/comment/1/up").should route_to("votes#vote_comment_up", :comment_id => '1')
    end

    it "routes to #vote_comment_down" do
      get("/vote/comment/1/down").should route_to("votes#vote_comment_down", :comment_id => '1')
    end

  end
end
