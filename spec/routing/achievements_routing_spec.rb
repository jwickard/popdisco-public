require "spec_helper"

describe AchievementsController do
  describe "routing" do

    it "routes to #index" do
      get("/achievements").should route_to("achievements#index")
    end

    it "routes to #show with id" do
      get("/achievements/4").should route_to("achievements#show", :id => "4")
    end

  end
end