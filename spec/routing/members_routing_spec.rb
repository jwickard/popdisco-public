require "spec_helper"

describe MembersController do
  describe "routing" do
    #TODO we should call these people pundits and not members
    it "routes to #index" do
      get("/members").should route_to("members#index")
    end

    it 'routes to #show' do
      get('/members/3').should route_to('members#show', :id => '3')
    end

    it 'routes to #recent_members' do
      get('/members/recent').should route_to('members#recent_members')
    end

    it 'routes to #users_by_reputation' do
      get('/members/reputation').should route_to('members#members_by_reputation')
    end
  end
end
